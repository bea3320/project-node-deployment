const mongoose = require('mongoose');
const DB_URL = process.env.DB_URL;
//mongodb://localhost:27017/semana-santa-proyecto
console.log('db url', process.env.DB_URL);
const connect = async() => {
    console.log('Testando la Base de Datos');
    try{
        mongoose.connect(DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        });
        console.log('Conectado a la Base de Datos');

    }catch(error) {
        console.log('Error conectado con la Base de Datos', error)
    }
}

module.exports = { connect, DB_URL }
