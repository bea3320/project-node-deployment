require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');
const MongoStore = require('connect-mongo');
const path = require('path');
const db = require('./db.js');
const indexRoutes = require('./routes/index.routes');
const jobsRoutes = require('./routes/jobs.routes');
const authRoutes = require('./routes/auth.routes');
const {isAuthenticated } = require('./middelware/auth.middelware');

db.connect();

require('./db.js');

const PORT= process.env.PORT || 3000;
const app = express();
const router = express.Router();

require('./passport/passport');

app.use(session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
        maxAge: 48 * 60 * 60 * 1000
    },
    store: MongoStore.create({ mongoUrl: db.DB_URL }),
}))

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static('public/assets'));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(passport.initialize())
app.use(passport.session());



app.use(express.json());

app.use(express.urlencoded({ extended: true }));

app.use(passport.initialize());
app.use('/', indexRoutes);
app.use('/jobs', [isAuthenticated], jobsRoutes);
app.use('/auth', authRoutes);

app.use('/', router);

app.use('*', (req, res, next) => {
    const error = new Error('Route not found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    console.log(error);
    return res.status(error.status || 500).render('error.hbs', {error: error.message || 'Unexpected error'});
})

/* app.use((error, req, res,next) => {
    console.log(error);
    error.message = error.message || 'Unexpected error';
    return res.status(error.status || 500).render('error',  {error});
}) */

app.listen(PORT, () => {
    console.log(`Server running in http://localhost:${PORT}`)
})