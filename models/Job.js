const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const jobSchema = new Schema({
    contactEmail: {type: String, required: true},
    title: {type: String, required: true},
    description: {type: String, required: true},
    company: {type: String, required: true},
    city:{type: String, required: true},
    image: {type: String},
    creatorId:[{type:mongoose.Types.ObjectId, ref:'User'}],
    jobId:[{type:mongoose.Types.ObjectId, ref: 'Job'}]
}, {
    timestamps: true
})

const Job = mongoose.model('Job', jobSchema);

module.exports = Job;

